package com.test.scala


import scala.io.Source
import java.io.File
import scala.collection.mutable.ListBuffer
import scala.math.BigDecimal.double2bigDecimal
import scala.math.random
import scala.util.control.Breaks.break
import scala.util.matching.Regex
//"testsss"
object game {
  def main(args: Array[String]): Unit = {
    val file = new File("src/main/resources")
    val absolutePath = file.getAbsolutePath
    val data = new ListBuffer[String]
    var rand = new ListBuffer[Int]
    getListOfFiles(absolutePath)
      .foreach((x)=> {
        for (line <- Source.fromFile(x).getLines) {
          data += line
        }
      })

    val i = 0
    for( i <- 1 to data.length/3){
        rand += i
    }

    println("Select Category")
    rand =  scala.util.Random.shuffle(rand).take(3)
    rand.foreach((i) => {
      println(data(i*3))
    })

    val input :Int= scala.io.StdIn.readInt()
    val numQuestion :Int =  rand(input-1)*3
    println("\n "+data(numQuestion+1))
    var pattern = "(?<=(Answer:)).*".r
    var Answer = data(numQuestion+2)
//    println(Answer)
    Answer = (pattern findAllIn Answer).mkString(",")
//sadasa

    val listAnswer = new ListBuffer[Char]
    Answer.foreach((i)=>{
      listAnswer += i
    })
    val listHit   = new ListBuffer[Int]
    val listReplace = new ListBuffer[Char]
    var count :Int =0
    listAnswer.foreach((i)=>{
      if(i.toString.matches("[^\\d\\W]")) {
        listHit += count
        listReplace += i

      }
      count +=1
    })
    listHit.foreach((i)=>{
      listAnswer(i) = '_'
    })
    var s = 0.0
    var limit = 10
    var wrong = ""
    var j = 0
    var score = 100/listReplace.length.toFloat

    while(limit>0){
      if (s>=99) {
        s = 100
      }
      if(limit == 10) {
        listAnswer.foreach(print)
        print ("   score " + s.setScale(2, BigDecimal.RoundingMode.HALF_UP) + ", remaining wrong guess " + limit + "\n")
      }else {
        listAnswer.foreach(print)
        print ("   score " + s.setScale(2, BigDecimal.RoundingMode.HALF_UP)+ ", remaining wrong guess " + limit + ", wrong guessed: " + wrong + "\n")
      }
      if(s>=99){
        break()
      }
      val userAnswer :Char= scala.io.StdIn.readChar()
      if(listReplace.contains(userAnswer)){
        j = listReplace.indexOf(userAnswer)
        println(j)
        listAnswer(listHit(j)) = listReplace(j)
        listHit.remove(j)
        listReplace.remove(j)
        s += score
      }else {
        wrong += " "+userAnswer
        limit -= 1
      }

    }




  }

  def getListOfFiles(dir: String): List[String] = {
    val file = new File(dir)
    file.listFiles.filter(_.isFile)
      .filter(_.getName.endsWith(".txt"))
      .map(_.getPath).toList
  }

}
