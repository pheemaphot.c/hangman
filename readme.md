Scala Hangman

This version of Hangman was written utilizing the Scala language, version 2.13.6

In this project, SBT or The simple Scala build tool was utilized to compile Hangman. To build Hangman, use this command:

sbt.bat compile
To run Hangman, invoke the following command:

sbt.bat run